package MainApplication;

import createChart.ChartPrinter;
import imageProcessing.ImageOperator;
import imageProcessing.PatternComparator;
import imageProcessing.PatternGenerator;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import featureExtraction.MFCC;
import featureExtraction.PreemphasisOfSignal;
import waveFileOperations.ReaderWAVE;
import waveFileOperations.WavFileException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AlgorithmicPart extends Application {

    enum WORD { KIEPE, KIPPE,
        ADER, ODER,
        BETTEN, BETEN,
        KÖCHE, KÜCHE,
        RÖSTEN, RÜSTEN,
        UMIFAHREN, UMFAHREN,
        SCHÄRE, SCHERE
    }

    private static ArrayList<File> createListOfFile(String pathname) throws IOException {
        List<File> contentOfFolder = null;
        ArrayList<File> filesFromFolder = null;

        try {
            contentOfFolder = Files.walk(Paths.get(pathname))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        filesFromFolder = new ArrayList<File>(contentOfFolder);

        return filesFromFolder;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        ArrayList<ArrayList<File>> contentsOfFolders = new ArrayList<ArrayList<File>>();
        ArrayList<File> contentFoldersToSave = null;
        ReaderWAVE readerToSave = null;
        ArrayList<ReaderWAVE> readers = new ArrayList<>();
        PreemphasisOfSignal preemphasedSignal = null;
        ArrayList<PreemphasisOfSignal> preparedSignals = new ArrayList<>();
        ArrayList<MFCC> mfccCoefficients = new ArrayList<>();
        final int SAMPLERATE = 44100;
        MFCC mfccToSave = null;
        ArrayList<ImageOperator> imagesOfWords = new ArrayList<>();
        ImageOperator images = null;
        ChartPrinter printer = new ChartPrinter();
        ArrayList<PatternGenerator> patterns = new ArrayList<>();
        PatternGenerator patternToAdd = null;
        final int AMOUNTOFPATTERNS = 4;

        ArrayList<String> pathnameOfFiles = new ArrayList<>(Arrays.asList(
                "C://Users/Kasia/Music/Engineers thesis/Kiepe/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/Kippe/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/Ader/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/oder/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/betten/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/beten/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/Köche/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/Küche/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/rösten/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/rüsten/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/umIfahren/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/umfahren/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/Schäre/wavFiles",
                "C://Users/Kasia/Music/Engineers thesis/Schere/wavFiles"
        ));

        ArrayList<String> pathnameOfOutputImages = new ArrayList<>(Arrays.asList(
                "C://Users/Kasia/Pictures/Engineers thesis/Kiepe/Kiepe",
                "C://Users/Kasia/Pictures/Engineers thesis/Kippe/Kippe",
                "C://Users/Kasia/Pictures/Engineers thesis/Ader/Ader",
                "C://Users/Kasia/Pictures/Engineers thesis/oder/oder",
                "C://Users/Kasia/Pictures/Engineers thesis/betten/betten",
                "C://Users/Kasia/Pictures/Engineers thesis/beten/beten",
                "C://Users/Kasia/Pictures/Engineers thesis/Köche/Köche",
                "C://Users/Kasia/Pictures/Engineers thesis/Küche/Küche",
                "C://Users/Kasia/Pictures/Engineers thesis/rösten/rösten",
                "C://Users/Kasia/Pictures/Engineers thesis/rüsten/rüsten",
                "C://Users/Kasia/Pictures/Engineers thesis/umIfahren/UmIfaren",
                "C://Users/Kasia/Pictures/Engineers thesis/umfahren/umfahren",
                "C://Users/Kasia/Pictures/Engineers thesis/Schäre/Schäre",
                "C://Users/Kasia/Pictures/Engineers thesis/Schere/Schere"
        ));

        try {
            for (String pathname: pathnameOfFiles) {
                contentFoldersToSave = new ArrayList<File>();
                contentFoldersToSave = createListOfFile(pathname);
                contentsOfFolders.add(contentFoldersToSave);
                contentFoldersToSave = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Invalid pathname!");;
        }

        try {
            for (ArrayList<File> filesFromFolder: contentsOfFolders) {
                readerToSave = new ReaderWAVE();
                readerToSave.detectionOfWords(filesFromFolder);
                readers.add(readerToSave);
                readerToSave = null;
            }
        } catch (IOException | WavFileException | NullPointerException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Invalid files!");;
        }

        /*printer.print(primaryStage,
                readers.get(0).detectedWords.get(3),
                0.001,
                "Kiepe");*/

        try {
            for (ReaderWAVE readerToRun: readers) {
                preemphasedSignal = new PreemphasisOfSignal();
                preemphasedSignal.preemphaseSignals(readerToRun.detectedWords);
                preparedSignals.add(preemphasedSignal);
                preemphasedSignal = null;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Invalid pathname!");;
        }

        try {
            for (PreemphasisOfSignal sample: preparedSignals) {
                mfccToSave = new MFCC(SAMPLERATE);
                mfccToSave.calculateMFCCCoefficients(sample.preemphasedSignals);
                mfccCoefficients.add(mfccToSave);
                mfccToSave = null;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Invalid pathname!");
        }

        printer.print(primaryStage, mfccCoefficients.get(0).window, 0.001, "Kiepe");

        int noOfWord = 0;

        for (MFCC word: mfccCoefficients) {
            images = new ImageOperator();
            images.setPathnames(pathnameOfOutputImages.get(noOfWord));
            images.createImage(word.normalizedMFCCCoefficients);
            imagesOfWords.add(images);
            images = null;
            noOfWord++;
        }

        noOfWord = 0;

        for (ImageOperator imagesOfManyWords: imagesOfWords) {
            patternToAdd = new PatternGenerator();
            patternToAdd.setPathnames(pathnameOfOutputImages.get(noOfWord));
            try {
                patternToAdd.createBounadryValues(imagesOfManyWords.rescaledImages, AMOUNTOFPATTERNS);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("You're choice too many patterns!");
            }
            patterns.add(patternToAdd);
            patternToAdd = null;
            noOfWord++;
        }

        PatternComparator comparator = new PatternComparator();
        comparator.setPatterns(patterns);
        comparator.scalePatterns();
        comparator.compareWordWithPercentage(imagesOfWords.get(5), WORD.BETEN.ordinal());
    }
}
